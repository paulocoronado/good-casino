package goodcasino;

public class Persona {
    protected  Ficha misFichas[];
    protected Dinero miDinero[];

    public Ficha[] getMisFichas() {
        return misFichas;
    }

    public void setMisFichas(Ficha[] misFichas) {
        this.misFichas = misFichas;
    }

    public Dinero[] getMiDinero() {
        return miDinero;
    }

    public void setMiDinero(Dinero[] miDinero) {
        this.miDinero = miDinero;
    }
    
    
}
